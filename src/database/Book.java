package database;

public class Book {
	public String isbn;
	public String title;
	public String price;
	
	public Book(String isbn, String title, String price) {
		super();
		this.isbn = isbn;
		this.title = title;
		this.price = price;
	}
	
	public String getIsbn() {
		return this.isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getTitle() {
		return this.title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPrice() {
		return this.price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
}
