package socket;

import java.net.*;
import java.util.ArrayList;
import java.util.Collection;

import database.*;
import java.io.*;

public class Server {
	private ArrayList<Book> books;
	
	public ArrayList<Book> getBooks() {
		return this.books;
	}
	
	public Server(){
		this.books = new ArrayList<Book>();
		Book b1 = new Book("9780545010221", "Harry Potter and the Deathly Hallows (Book 7)", "11.37");
		Book b2 = new Book("9780439784542", "Harry Potter and the Half-Blood Prince (Book 6)", "14.95");
		Book b3 = new Book("9780439139595", "Harry Potter And The Goblet Of Fire (Book 4)", "15.95");
		Book b4 = new Book("9780439136358", "Harry Potter And The Prisoner Of Azkaban (Book 3)", "13.90"); 
		Book b5 = new Book("9780439064866", "Harry Potter and the Chamber of Secrets (Book 2)", "6.64");
		Book b6 = new Book("9780590353403", "Harry Potter And The Sorcerer's Stone (Book 1)", "11.50");
		Book b7 = new Book("9780439358064", "Harry Potter and the Order of the Phoenix (Book 5)", "23.05");
		
		books.add(b1);
		books.add(b2);
		books.add(b3);
		books.add(b4);
		books.add(b5);
		books.add(b6);
		books.add(b7);
	}
	
	public static void main(String argv[]) throws Exception {
		Server s = new Server();
		String clientISBN; 
		ServerSocket welcomeSocket = new ServerSocket(6789);
		while(true) {
			Socket connectionSocket = welcomeSocket.accept(); 
			BufferedReader inFromClient = new BufferedReader(
					new InputStreamReader(connectionSocket.getInputStream())); 
			DataOutputStream outToClient = new DataOutputStream(
					connectionSocket.getOutputStream());
			clientISBN = inFromClient.readLine();
			
			ArrayList<Book> books = s.getBooks();
			boolean exist = false;
			for (Book book: books) {
//				System.out.println(book.getIsbn());
				if (book.getIsbn().equals(clientISBN)) {
					outToClient.writeBytes("Title: " + book.getTitle() + "\n");
					outToClient.writeBytes("Price: " + book.getPrice() + "\n");
					exist = true;
					break;
				}
			}
			if (!exist) {
				outToClient.writeBytes("Not exist!!!!");
			}
		}
	}
}
