package socket;

import java.io.*; 
import java.net.*;

public class Client {
	public static void main(String argv[]) throws Exception {
		String ISBN, title, price;
		
		System.out.print("Insert ISBN code: ");
		BufferedReader inFromUser = new
				BufferedReader(new InputStreamReader(System.in));
		
		Socket clientSocket = new Socket("localhost", 6789);
		DataOutputStream outToServer = new
				 DataOutputStream(clientSocket.getOutputStream());
		BufferedReader inFromServer = new BufferedReader(
				new InputStreamReader(
						clientSocket.getInputStream()));
		
		ISBN = inFromUser.readLine(); 
		outToServer.writeBytes(ISBN + '\n');
		title = inFromServer.readLine();
		System.out.println("FROM SERVER: ");
		System.out.println(title); 
		price = inFromServer.readLine();
		System.out.println(price); 
		clientSocket.close();
	}
}
