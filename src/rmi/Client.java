package rmi;

import java.io.*;
import java.rmi.registry.*;
import rmi.GetBookDetails;
public class Client {
	private Client() {};
	public static void main(String[] args) {
		String isbn;
		try {
			System.out.print("Insert ISBN code: ");
			BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
			isbn = inFromUser.readLine();
			
			Registry registry = LocateRegistry.getRegistry();
			GetBookDetails stub = (GetBookDetails)registry.lookup("GetBookDetails");
			String response = stub.getBookDetails(isbn);
			System.out.println("response from server: " + response);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
