package rmi;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface GetBookDetails extends Remote {
	String getBookDetails(String isbn) throws RemoteException;
}