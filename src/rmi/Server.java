package rmi;

import rmi.GetBookDetails;
import database.Book;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.*;
import java.util.ArrayList;


import database.Book;

public class Server implements GetBookDetails {
	private ArrayList<Book> books;

	public Server(){
		this.books = new ArrayList<Book>();
		Book b1 = new Book("9780545010221", "Harry Potter and the Deathly Hallows (Book 7)", "11.37");
		Book b2 = new Book("9780439784542", "Harry Potter and the Half-Blood Prince (Book 6)", "14.95");
		Book b3 = new Book("9780439139595", "Harry Potter And The Goblet Of Fire (Book 4)", "15.95");
		Book b4 = new Book("9780439136358", "Harry Potter And The Prisoner Of Azkaban (Book 3)", "13.90"); 
		Book b5 = new Book("9780439064866", "Harry Potter and the Chamber of Secrets (Book 2)", "6.64");
		Book b6 = new Book("9780590353403", "Harry Potter And The Sorcerer's Stone (Book 1)", "11.50");
		Book b7 = new Book("9780439358064", "Harry Potter and the Order of the Phoenix (Book 5)", "23.05");
		
		books.add(b1);
		books.add(b2);
		books.add(b3);
		books.add(b4);
		books.add(b5);
		books.add(b6);
		books.add(b7);
	}

	public ArrayList<Book> getBooks() {
		return books;
	}

	public void setBooks(ArrayList<Book> books) {
		this.books = books;
	}
	
	@Override
	public String getBookDetails(String isbn) throws RemoteException {
		// TODO Auto-generated method stub
		for(Book temp : books) {
			if(temp.getIsbn() == isbn) {
				return "title: " + temp.getTitle() + " and price: " + temp.getPrice(); 
			}
		}
		return "requested book not found on server!";
	}
	
	public static void main(String args[]) {
		Server server = new Server();
		try {
			GetBookDetails stub = (GetBookDetails)UnicastRemoteObject.exportObject(server, 0);
			Registry registry = LocateRegistry.getRegistry();
			registry.bind("GetBookDetails", stub);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
	}
}
